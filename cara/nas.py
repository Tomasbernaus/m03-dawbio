from turtle import *

# --------------- Draw nose ---------------
def draw_nose(  origin_x: int, origin_y: int,
                des_a: int,    des_b: int,
                des_c: int,    des_d: int):
    "Draws a nose. Requires three points: origin = tip of the nose; des_a, b = upper nose, des_c,d = lower nose."

    # First line
    penup()
    goto(origin_x, origin_y)
    pendown()
    goto(des_a, des_b)

    # Second line
    penup()
    goto(origin_x, origin_y)
    pendown()
    goto(des_c, des_d)
