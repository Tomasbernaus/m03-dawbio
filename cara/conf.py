from turtle import *

# --------------- Configuration ---------------
def conf_canv(canvas_height:int, canvas_width:int):
    "Defines the canva's configuration"
    window_top_left_x = 0
    window_top_left_y = 0

    setup(canvas_width,
          canvas_height,
          window_top_left_x,
          window_top_left_y)
