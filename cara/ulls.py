from turtle import *

# --------------- Draw eyes ---------------
def draw_eye(origin_x:int, origin_y:int, size_out:int, size_in:int):
    "Draws the eyes"

    penup()
    
    goto(origin_x,
         origin_y)

    pendown()
    circle(size_out)
    circle(size_in)
