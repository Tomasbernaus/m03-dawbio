from turtle import *


# --------------- Draw shape ---------------
def draw_circle(origin_x:int, origin_y:int,size:int):
    "Draws the shape of the face"

    penup()
    
    goto(origin_x,
         origin_y)

    pendown()
    circle(size)

