from turtle import *


# --------------- Draw mouth ---------------
def draw_mouth(origin_x:int, origin_y:int, des_x:int, des_y:int, des_z):
    "Draws a mouth"
    penup()
    goto(origin_x, origin_y)

    pendown()
    goto(des_x, des_y)
    goto(des_y, des_z)
    
    