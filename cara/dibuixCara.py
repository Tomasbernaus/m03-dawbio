from turtle import *

from ulls import draw_eye
from boca import draw_mouth
from nas import draw_nose
from forma import draw_circle
from conf import conf_canv


# --------------- Draw design ---------------
def draw_face():
    "Draws the face"
    speed("fastest")
    conf_canv(400,500)
    draw_circle(0,-200,200)
    draw_eye(100,0,60,40)
    draw_eye(-100,0,60,40)
    draw_mouth(10,-100,-100,-120,-100)
    draw_nose(10, -10, 0, 20, -15, -10)
    penup()
    goto(500,500)
    done()

# --------------- Do it ---------------
draw_face()
