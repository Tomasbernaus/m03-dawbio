from new_engine import create_html_output, get_data_paths, get_data_to_fill, get_html_templates

def parse_html_files():

    markdown_list = get_data_paths()
    html_templates = get_html_templates()
    markdown_data = get_data_to_fill(markdown_list)
    create_html_output(markdown_data, html_templates)



if __name__ == "__main__":

    parse_html_files()
