---
date: 
title: Second step!
subtitle: 
route: Home
type: steps
img: ./img/step_2.png
---

Then, we create the central data structure based on markdown data: a nested dictionary that is able to classify the route, type and content on the fly. With this, selecting data in HTML templates becomes easier