# M03 - Programación
## Práctica 2


# Introducción

La práctica consiste en la mejora del SSG (Static Site Generator) que ha facilitado Pablo.

Token de acceso:
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

# Qué ha facilitado Pablo?

El contenido se basa en un SSG basado en python y se compone de:

- (1) Plantilla HTML, CSS, JS (vacío) y distintos ficheros MD que se integran en Python

- (2) Motor Python

- (3) Carpeta de salida: con el código HTML, CSS i MD todo compilado

# Cosas de trabajo

- Aymane tiene una plantilla HTML i CSS para empezar


# Para ver donde ha caído el markdown parseado

print(markdown_data)

Copiar pegar en la siguiente web:

https://codebeautify.org/python-formatter-beautifier
