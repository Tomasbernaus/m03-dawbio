
from pathlib import Path

''' Exercise: Count the total for dosi_1 and dosi_2 in the simple dataset.
    v2: Full solution for the simple covid dataset.'''


# 1. Read csv file
# -----------------------------------------------------------------------------
def read_csv(csv_file_path: str) -> str:
   '''Input:  The path to a .csv file.
      Output: The contents of the .csv file as a single string.'''

   raw_text:      str = Path(csv_file_path).read_text()
   stripped_text: str = raw_text.strip()

   return stripped_text


# 2. Separate by rows
# -----------------------------------------------------------------------------
def separate_by_rows(contents: str) -> list[str]:
   '''Input:  The file contents as a single string.
      Output: A list of strings where each string is a row of the csv file.'''
   
   rows: list[str] = contents.split("\n")

   return rows


# 3. Separate by columns
# -----------------------------------------------------------------------------
def separate_by_columns(rows: list[str]) -> list[list[str]]:
   '''Input:  A list of strings. Each string is a row of a csv file. Row fields are separated by ";".
      Output: A table where each row has been splitted into a list of fields.'''
   
   table: list[list[str]] = []
   row:   str

   for row in rows:
      splitted_row: list[str] = row.split(";")
      table.append(splitted_row)

   return table


# 4. Get the Full Names
# -----------------------------------------------------------------------------
def get_names(table: list[list[str]]) -> list[list[int]]:
   '''Input:  A table with members Data. Each datum is a string.
      Output: The full name of all members: "first_name" and "last_name" as "full_name"'''
   
   header:     list[str]       = table[0]
   person_data: list[list[str]] = table[1:]
   row:        list[str]

   first_name: str = header.index("FIRSTNAME")
   last_name: str = header.index("LASTNAME")

   fname_list: list[str] = []
   lname_list: list[str] = []
   full_name_list: list[str] = []

   for row in person_data:
      fname_str: str = row[first_name]
      fname_list.append(fname_str)

      lname_str: str = row[last_name]
      lname_list.append(lname_str)

      full_name_list.append(fname_str+" "+lname_str)

   return full_name_list


# 5. Other informations about the team members
# -----------------------------------------------------------------------------
def participation(indexes: int) -> int:
   '''Input: Input:  A table with members Data. 
      Output: The requested data from the members'''
   header:     list[str]       = table[0]
   person_data:list[list[str]] = table[1:]
   row:        list[str]
   if indexes==1:
      option: str = header.index("GROUP")
   elif indexes==2:
      option: str = header.index("GENDER")
   elif indexes==3:
      option: str = header.index("PARTICIPATION")
   else:
      option: str = header.index("COMENTARI")
 

   member_data: list[str] = []

   for row in person_data:
      member_info: str = row[option]
      member_data.append(member_info) 

   return member_data



# Main
# -----------------------------------------------------------------------------
# values
indexes=int(input(f"Select what you want to know about the members:\n0: Full Name\n1: GROUP\n2: GENDER\n3: PARTICIPATION\n4: COMENTARY\n"))

contents:   str                 = read_csv("./13-tables/About.csv")
rows:        list[str]          = separate_by_rows(contents)
table:       list[list[str]]    = separate_by_columns(rows)
persons:     list[list[int]]    = get_names(table)
header:      list[list[str]]    = table[0]
information: list[str]          = participation(indexes)

#Print
i=0
if indexes in range (1,5):
   print(f"{header[0]}, {header[1]}: {header[indexes+1]}")
   for person in persons:
      print(f"{person}: {information[i]}")
      i=i+1

elif indexes == 0:   
      for person in persons:
         print(f"{person}")
         i=i+1

else:
   print(f"Not a correct value")
# -----------------------------------------------------------------------------
