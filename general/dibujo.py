from turtle import *

setup(400,400,0,0)

penup()
goto(100,0)

pendown()
circle(60)
circle(40)

penup()
goto(-100,0)

pendown()
circle(60)
circle(40)

penup()
goto(100,-100)

pendown()
goto(-100,-100)

penup()
goto(0,-10)

pendown()
goto(0,10)

penup()
goto(500,500)
