from turtle import *


# --------------- Configuration ---------------
def conf_canv(canvas_height, canvas_width):
    "Defines the canva's configuration"
    window_top_left_x = 0
    window_top_left_y = 0

    setup(canvas_width,
          canvas_height,
          window_top_left_x,
          window_top_left_y)


# --------------- Draw shape ---------------
def draw_shape(origin_x, origin_y,size):
    "Draws the shape of the face"

    penup()
    
    goto(origin_x,
         origin_y)

    pendown()
    circle(size)


# --------------- Draw eyes ---------------
def draw_eye(origin_x, origin_y, size_out, size_in):
    "Draws the eyes"

    penup()
    
    goto(origin_x,
         origin_y)

    pendown()
    circle(size_out)
    circle(size_in)


# --------------- Draw mouth ---------------
def draw_mouth(origin_x, origin_y, des_x, des_y, des_z):
    "Draws a mouth"

    penup()
    goto(origin_x, origin_y)

    pendown()
    goto(des_x, des_y)
    goto(des_y, des_z)
    
    
# --------------- Draw nose ---------------
def draw_nose(  origin_x: int, origin_y: int,
                des_a: int,    des_b: int,
                des_c: int,    des_d: int):
    "Draws a nose. Requires three points: origin = tip of the nose; des_a, b = upper nose, des_c,d = lower nose."

    # First line
    penup()
    goto(origin_x, origin_y)
    pendown()
    goto(des_a, des_b)

    # Second line
    penup()
    goto(origin_x, origin_y)
    pendown()
    goto(des_c, des_d)


# --------------- Draw design ---------------
def draw_face():
    "Draws the face"
    conf_canv(400,500)
    draw_shape(0,-200,200)
    draw_eye(100,0,60,40)
    draw_eye(-100,0,60,40)
    draw_mouth(10,-100,-100,-120,-100)
    draw_nose(10,-10, 0,20, -15, -10)
    penup()
    goto(500,500)


# --------------- Do it ---------------
speed("fastest")
draw_face()
done()
