# import pprint


#-----------------------------------string creation-----------------------------------

# s1: str = "Esto es un string. Un string es un texto."
# s2: str = 'Esto es un string. Un string es un texto.'
# s3: str = "Comillas simples y comillas dobles son idénticas en python."
# s4: str = "A" #String de una sola letra, no hay tipo char en python.
# s5: str = 'It\'s a trap!' #Escape characters
# s6: str = """Comillas triples, respetan saltos de línea (pueden ser dobles (") o simples ('))"""
# print(s1, s2, s3, s4, s5, s6)



#-----------------------------------substrings-----------------------------------

# s1: str = "Hello"
# print(s1[0:len(s1)]) 
# si no hay número antes del [:X] se hace desde el inicio y si no hay después [X:] es hasta el final
# print (len(s1)) --> print 5 (tamaño s1 (Hello))



#-----------------------------------Strings are inmutable-----------------------------------

# s1: str = "Hello"
# s2: str = s1.upper()
# print (s2)



#-----------------------------------Repetición-----------------------------------

# print ("Hello world " *3)



#-----------------------------------Índices negativos-----------------------------------

# no lo usaremos
# s2:str = ("Python")
# print(s2[-3])


