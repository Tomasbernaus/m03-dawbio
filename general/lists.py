import pprint
import random
import copy

#array --> secuencia de elementos de cualquier tipo

#Official tutorial
#Use python 3.9 to use native types subtyping



#-----------------------------------Lists-----------------------------------

s1: str = "Hello World" #Literal, directamente el string


#literal lists
l1: list = [] #empty list
l2: list[str] = ["nombre 1", "nombre 2", "nombre 3"] #lista de strings
l2_len: int = len(l2)
#print (l2[len(l2)-1]) // print(l2[2]) // print(l2[-1]) // <-- todo devuelve nombre 3
#print (l2[2][0]) <-- devuelve la primera letra del nombre 3 (en este caso, la letra "n")



#element acces
# l6: list 2]



#sublists (slices)
l3:list[str]=["a", "b", "c", "d", "e", "f"]
#print (l3[0:4]) // <-- devuelve "a, b, c, d"
#print (l3[4:]) // <-- devuelve "e, f"



#Assignments. Lists are mutable!
l3[0]="z"
l3[0]="a"



#List operations +, *
l4:list[str]=["nombre 1", "nombre 2"] + ["nombre 3", "nombre 4"]



#Slice assignments
l3[3:] = [1,2,3] #d, e, f pasa a ser 1, 2, 3
l3[3:] = [] #delete everything from index 3



#list of lists
l5:list[list[int]]=[[1,2],[3,4]]
first_row:list[int]=l5[0]
second_row:list[int]=l5[1]
# print(f"{first_row}\n{second_row}") <-- imprime las dos listas con un salto de línea entre ellos




#In Python, all functions return smt
#If there's no "return" keyword, they return None.
#A function is pure if...
# 1.- only reads from its input parameters
# 2.- only writes its output parameters
# 3.- given the same input parameters it always outputs the same output parameters (Always gives the exact same result when given the same inputs)

#Examples: failure of condition 1

# a=1

# def do_smt_v1():
#     print(a)

# do_smt_v1()



# def do_smt_v2():
#     b=2
#     print(b)

# do_smt_v2

#Examples: failure of condition 2

# l6:list[int]=[1, 2, 3]
# l6.append(4)

#Examples: failure of condition 3

# def do_smt_v3():
#     return random.randint(0,5)
    
# do_smt_v3()



# Methods lists
# l6:list[int]=[0,1,2,3,4,5]
# l6_methods: list[str] = dir(l6)


# Append does not return anything!! 
# l6.append(6)
# result=l6.append
# print(result)


#Extend
l6:list[int]=[1,2,3]
l7:list[int]=[4,5,6]
l8:list[int]=l7+l6 #Pure operation! Always prefered before the extend operator
l6.extend(l7)


#Insert. does not overwrite
l6.insert(len(l6), 7) #insert (posición, cosa a insertar)


#Remove. uses the element to be removed, not the index (the first element found)
l6.remove(7)


#Pop. removes by index
l6.pop(len(l6)-1)


#Index. search elements //Pure Function\\
# l6.index(1)


#Count //Pure Function\\
# l6.count(2)


#Clear. Removes all the elements but the list
# l6.clear()


#Del. Del is a keyword, not a function. (does not need brackets "()"). Can delete anything
# del l6[len(l6)-1] #delete an element
# del l6[:] #delete all the elements of the array
# del l6 #delete the array



#-----------------------------------//Pure Function\\-----------------------------------



#Sorted.  //Pure Function\\
l6_sorted:list[int]=sorted(l6, reverse=True) #sorts the array, reverse=true --> reverse the sorted array

#Reversed //Pure Function\\
l6_reversed: list[int]=list(reversed(l6)) 


#Copy //Pure Function\\. It does also exist .deepcopy()
l9:list[int]=copy.copy(l6)





#-----------------------------------A-----------------------------------
print(l9)
#-----------------------------------A-----------------------------------

