class Square:
    x:      float
    y:      float
    length: float

    # Constructor
    def __init__(self, x: float, y: float, length: float): 
        "Constructor"
        
        self.x = x
        self.y = y
        self.length = length

#-----------------------------------------------------------------------------------------------------------------
    def get_upper_left_x(self) -> float: 
        "Esta función devuelve un vértice del cuadrado"

        midlength: float = self.length/2
        upper_left_x = self.x + midlength
        upper_left_y = self.y + midlength
        result = upper_left_x, upper_left_y

        return result
        
#-----------------------------------------------------------------------------------------------------------------
    def get_upper_right_x(self) -> float: 
        "Esta función devuelve un vértice del cuadrado"

        midlength: float = self.length/2
        upper_right_x = self.x - midlength
        upper_right_y = self.y + midlength
        result = upper_right_x, upper_right_y

        return result


s: Square = Square(0,0,10)
print("atributos del cuadrado s:")
print(s.x, ", ", s.y, ", ", s.length)
print("Vertice superior izquiero: ")
print(s.get_upper_left_x())
print("Vertice superior derecho: ")
print(s.get_upper_right_x())