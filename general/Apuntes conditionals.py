
#------------------------------------------

num_list: list = [0, 1, 2]
if (len(num_list) == 4):
    print("Hay 4 o más elementos! Voy a imprimirlos.")
    print(num_list)


else:
    print("Hay menos de 4 elementos. POn más elementos por favor.")


def new_func():

    #Ejemplo IF ANIDADO -----------------------------------------

    edad: int = 25  # Años

    if (0 <= edad <= 1):  # If else
        print("Regalar pañales")
    else:
        #edad >1
        if (1 <= edad <= 6):
            print("Regalar pelota")
        else:
            #edad >6
            if (6 <= edad <= 90):
                print("Regalar ropa")
            else:
                if (90 < edad):
                    #edad > 90
                    print("Regalar pañales")


new_func()
