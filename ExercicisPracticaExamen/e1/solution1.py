# """
# Exercici 1:

# - Fes un programa que enviï invitacions a una llista d'amics.
# - Utilitza engine.py
# - Utilitza els fitxers friends.txt i letter.txt
# - Modifica letter.txt utilitzant Jinja.
# """

from pathlib import Path
import engine 

archivo = open("/home/tomas/Desktop/bitbucket/m03-dawbio/practicaExamen/e1/1-questions/friends.txt", "r")
friends = archivo.read()
listaAmigos = friends.split("\n")
print(listaAmigos)

for amigo in listaAmigos:  
    # engine.fill_template()
    print(f"Hola {amigo}")