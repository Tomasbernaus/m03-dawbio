# Tomàs Bernaus, exercici 3
# Creación de un visor de imágenes en jinja2
#
# plantejament: 
# 1.- llegir totes les fotos de la carpeta
# 2.- fer una llista amb les fotos
# 3.- substituir la plantilla per fer la pàgina amb aquestes fotos
# 4.- fer una llista amb les plantilles
# 5.- imprimir la llista a un arxiu


import glob
from pathlib import Path
import engine

def read_hamsters(hamsters_filename: str) -> list[str]:
    '''2.- fer la llista amb les fotos'''

    hamsters_filepath: Path      = Path(hamsters_filename)
    hamsters_text:     str       = hamsters_filepath.read_text()
    hamsters_list:     list[str] = hamsters_text.strip().split()

    return hamsters_list


def make_templates(hamsters_list: list[str], letter_template_filename: str):
    '''3.- substituir les plantilles per les fotos + 4.- fer la llista amb les plantilles'''
    main_html: list = []

    for hamster in hamsters_list:
        template_dir: Path = Path('.')
        vars_dict:    dict = {'hamster': hamster}

        invitation:   str  = engine.fill_template_file(template_dir, letter_template_filename, vars_dict)
       
        main_html.append(invitation+'\n')

    print(main_html)
    return main_html


def make_html(main_html):
    '''5.- imprimir la llista a un sol arxiu'''
    cosa:str=('')
    f = Path('./index.html')
    index_text: str = cosa.join(map(str,main_html))
    f.write_text(index_text)



hamsters_list: list[str] = read_hamsters('hamsters.txt')
main_html:     list[str] = make_templates(hamsters_list, 'template.txt')
make_html(main_html)