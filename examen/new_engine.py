from collections import defaultdict
from pathlib import Path
import markdown
import jinja2 as j
import shutil

# Path functions: returns paths of needed files


def get_data_paths() -> list[str]:
    """
    Returns a list with Path objects of the data to fill HTML templates
    No input arguments needed
    """

    # Returned list
    files_list: list[list[str, str]] = []

    current_path: Path = Path(__file__).parent

    template_data_path: Path = current_path / "input/template_data"

    file_iterator: Path = template_data_path.iterdir()

    # A list of tuple(absolute path, file extension) is returned
    files_list: list[str] = [file.resolve() for file in file_iterator]

    routing_files: list[str] = [
        file for file in files_list if file.suffix == ".txt"]

    assert len(files_list) > 0, "Error! No data to fill found!"
    assert len(routing_files) <= 1, "Only one (or 0) file is accepted for routing. All .txt files are interpreted as router files"

    files_list.sort()

    return files_list


def get_html_templates() -> list[list[str, str]]:
    """
    Return a list of tuples: (template absolute path, type)
    """

    html_templates: list[list[str, str]] = []

    current_path: Path = Path(__file__).parent

    template_path: Path = current_path/"input/html"

    template_iter = template_path.iterdir()

    html_templates = [(template.resolve(), template.stem.split("_")[0]) for template in template_iter]

    return html_templates

# Data parsing: files from paths are processed


def get_data_to_fill(file_list: list[str]) -> dict(str=dict(str=dict(str=dict(str=str)))):
    """
    Reads the content of the given paths and returns a dictionary with the parsed data to pas to Jinja and HTML templates

    Dictonary Schema:
        (1) MARKDOWN or ROUTE file
        ----markdown:
            ----route:
                ----type:
                    ----md file:
                        ----meta: meta content of md
                        ----content: content parsed to HTML

        (2) ROUTES: [route 1, route 2, route n...]
    """

    md = markdown.Markdown(extensions=["meta"])

    markdown_data = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))

    # markdown_data = {
    #     "markdown": {
    #         "Home": {"post": {}, "news": {}},
    #         "Shop": {"post": {}, "news": {}},
    #         "About": {"post": {}, "news": {}}
    #          ....
    #     },
    #     "routes": []
    # }

    for file in file_list:

        is_md: bool = file.suffix == ".md"
        is_txt: bool = file.suffix == ".txt"

        if is_md:
            str_md: str = file.read_text()
            parsed_md = md.convert(str_md)

            route: str = md.Meta["route"][0]
            type_of_content: str = md.Meta["type"][0]

            markdown_data["markdown"][route][type_of_content][file.stem] = {
                "meta": md.Meta, "content": parsed_md
            }

        elif is_txt:
            routes: list[str] = file.read_text().split(";")
            markdown_data["routes"] = routes

    return dict(markdown_data)

# Outputs


def create_html_output(markdown_data: dict, html_templates: list[list[str, str]]) -> None:
    """
        Returns a list of tuples of:
        [(filled HTML to write, name of the output file)]
    """

    current_path: Path = Path(__file__).parent
    input_path: Path = current_path/"input"
    output_path: Path = current_path/"output"
    templates_path: Path = current_path/"input/html"

    # Create output dirs
    output_path.mkdir(exist_ok=True)
    (output_path/"css").mkdir(exist_ok=True)
    (output_path/"img").mkdir(exist_ok=True)
    (output_path/"js").mkdir(exist_ok=True)

    shutil.copytree(input_path/"css", output_path/"css", dirs_exist_ok=True)
    shutil.copytree(input_path/"img", output_path/"img", dirs_exist_ok=True)
    shutil.copytree(input_path/"js",  output_path/"js",  dirs_exist_ok=True)

    jinja_data: dict = {"entry_list": markdown_data}

    env: j.Environment = j.Environment(
        loader=j.FileSystemLoader(templates_path),
        autoescape=j.select_autoescape()
    )

    for template in html_templates:

        routing: str = template[1]

        if routing == "Home":
            routing = "index"

        template: j.Template = env.get_template(template[0].stem+".html")

        filled_str: str = template.render(jinja_data)

        output_file: Path = (output_path/routing).with_suffix(".html")

        output_file.write_text(filled_str)

        print(f"{routing} has been written!")