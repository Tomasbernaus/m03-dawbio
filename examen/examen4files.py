#Creación de un visor de imágenes en jinja2

from   pathlib import Path
import engine

def read_hamsters(hamsters_filename: str) -> list[str]:

    hamsters_filepath: Path      = Path(hamsters_filename)
    hamsters_text:     str       = hamsters_filepath.read_text()
    hamsters_list:     list[str] = hamsters_text.strip().split()

    return hamsters_list

def write_invitations(hamsters_list: list[str], letter_template_filename: str):

   for hamster in hamsters_list:

      template_dir: Path = Path('.')
      vars_dict:    dict = {'hamster': hamster}

      invitation:   str  = engine.fill_template_file(template_dir, letter_template_filename, vars_dict)

      invitation_filename: str  = f'{hamster}.html'
      invitation_filepath: Path = Path(invitation_filename)
      invitation_filepath.write_text(invitation)




hamsters_list: list[str] = read_hamsters('hamsters.txt')
write_invitations(hamsters_list, 'template.txt')