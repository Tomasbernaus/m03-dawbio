# Tomàs Bernaus, exercici 3
# Creación de un visor de imágenes en jinja2
#
# plantejament: 
# 1.- llegir totes les fotos de la carpeta
# 2.- fer una llista amb les fotos
# 3.- substituir la plantilla per fer la pàgina amb aquestes fotos
# 4.- fer una llista amb les plantilles
# 5.- imprimir la llista a un arxiu


from pathlib import Path
import engine
import sys


def search_pictures(carpeta: Path) -> list[str]:
    '''1.- llegir les fotos de la carpeta + 2.- fer la llista amb les fotos'''
    
    images:       Path      = Path(carpeta).glob("*.jpg")
    picture_list: list[str] = [str(imgs) for imgs in images]
            
    return picture_list


def make_templates(picture_list: list[str], letter_template_filename: str) -> list[str]:
    '''3.- substituir les plantilles per les fotos + 4.- fer la llista amb les plantilles'''
    main_html: list = ['<head>\n</head>\n<body>\n']

    for picture in picture_list:

        template_dir: Path = Path('.')
        vars_dict:    dict = {'imagen': picture}
        picture:      str  = engine.fill_template_file(template_dir, letter_template_filename, vars_dict)
        
        main_html.append(picture+'\n')

    main_html.append('</body>')
    return main_html
#Añado el head y el body del html de forma cutre y rápida por falta de tiempo, falta hacer un bucle en el template para que aparezca de forma correcta


def make_html(main_html: list[str]):
    '''5.- imprimir la llista a un sol arxiu'''
    index_txt:  str  =('')
    html_file:  Path = Path('./index.html')
    index_text: str  = index_txt.join(map(str,main_html))
    html_file.write_text(index_text)


if __name__ == "__main__":
    carpeta:      Path      = (str(sys.argv[1]))
    picture_list: list[str] = search_pictures(carpeta)
    main_html:    list[str] = make_templates(picture_list, 'template.txt')
    make_html(main_html)

