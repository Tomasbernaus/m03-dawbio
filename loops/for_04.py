# 7 .- Create an array with names and another one empty

names: list[str] = []
new_names: list[str] = []

print("Write your names:")

name1 = input()
names.append(name1)

name2 = input()
names.append(name2)

name3 = input()
names.append(name3)

print()

for name in names:
    new_names.append(name)

print()

print(new_names==names)