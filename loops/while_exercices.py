# https://pynative.com/python-if-else-and-for-loop-exercise-with-solutions/#h-exercise-1-print-first-10-natural-numbers-using-while-loop


#-----------------------------------EXERCICE 1-----------------------------------

# Print first 10 natural numbers

#-------------------------My Solution-------------------------
# cosas:list[int] = [1,2,3,4,5,6,7,8,9,10]
# for cosa in cosas:
#     print(cosa)
#-------------------------PyNative Solution-------------------------
# i = 1
# while i <= 10:
#     print(i)
#     i += 1


#-----------------------------------EXERCICE 2-----------------------------------

# Exercise 2: Print the following pattern
# 1 
# 1 2 
# 1 2 3 
# 1 2 3 4 
# 1 2 3 4 5
# Write a program to print the following number pattern using a loop.

#-------------------------My Solution-------------------------
# numeros:list[int] = [1,2,3,4,5]
# numeros2:list[int]=[]
# durada:int=len(numeros)
# i:int=0
# while durada > i:
#     numeros2.append(numeros[i])
#     print(f"{numeros2}")
#     i=i+1

#-------------------------PyNative Solution-------------------------
# print("Number Pattern ")
# Decide the row count. (above pattern contains 5 rows)
# row = 5
# start: 1
# stop: row+1 (range never include stop number in result)
# step: 1
# run loop 5 times
# for i in range(1, row + 1, 1):
#     # Run inner loop i+1 times
#     for j in range(1, i + 1):
#         print(j, end=' ')
#     # empty line after each row
#     print("")


#-----------------------------------EXERCICE 3-----------------------------------

# Calculate the sum of all numbers from 1 to a given number
# Write a program to accept a number from a user and calculate the sum of all numbers from 1 to a given number
# For example, if the user entered 10 the output should be 55 (1+2+3+4+5+6+7+8+9+10)

#-------------------------My Solution-------------------------
# entrada = int(input("Número a sumar: "))
# suma: int = 0
# while entrada > 0:
#     suma = suma + entrada
#     entrada=entrada-1
# print(f"El resultado es: {suma}")
#-------------------------PyNative Solution-------------------------
# # s: store sum of all numbers
# s = 0
# n = int(input("Enter number "))
# # run loop n times
# # stop: n+1 (because range never include stop number in result)
# for i in range(1, n + 1, 1):
#     # add current number to sum variable
#     s += i
# print("\n")
# print("Sum is: ", s)


#-----------------------------------EXERCICE 4-----------------------------------

#  Write a program to print multiplication table of a given number

# For example, num = 2 so the output should be
# 2
# 4
# 6
# 8
# 10
# 12
# 14
# 16
# 18
# 20

#-------------------------My Solution-------------------------
# entrada = int(input("Veamos la tabla del "))
# reserva:int = 0
# total: int= 0
# while total < 10:
#     reserva=reserva+entrada 
#     print(f"{reserva}")
#     total=total+1
#-------------------------PyNative Solution-------------------------
# n = 2
# # stop: 11 (because range never include stop number in result)
# # run loop 10 times
# for i in range(1, 11, 1):
#     # 2 *i (current number)
#     product = n * i
#     print(product)


#-----------------------------------EXERCICE 5-----------------------------------

# Display numbers from a list using loop

# Write a program to display only those numbers from a list that satisfy the following conditions

#     The number must be divisible by five
#     If the number is greater than 150, then skip it and move to the next number
#     If the number is greater than 500, then stop the loop

# Given:

# numbers = [12, 75, 150, 180, 145, 525, 50]

# Expected output:

# 75
# 150
# 145

#-------------------------My Solution-------------------------
# numbers = [12, 75, 150, 180, 145, 525, 50]
# total: int = len(numbers)
# i: int = 0
# while total > i:
#     if numbers[i]%5 == 0 and numbers[i] <= 150:
#         print(f"{numbers[i]}")
#     i=i+1
#     if numbers[i] > 500:
#         i=len(numbers)
#-------------------------PyNative Solution-------------------------
# numbers = [12, 75, 150, 180, 145, 525, 50]
# # iterate each item of a list
# for item in numbers:
#     if item > 500:
#         break
#     elif item > 150:
#         continue
#     # check if number is divisible by 5
#     elif item % 5 == 0:
#         print(item)


#-----------------------------------EXERCICE 6-----------------------------------

# Count the total number of digits in a number

# Write a program to count the total number of digits in a number using a while loop.

# For example, the number is 75869, so the output should be 5.

#-------------------------My Solution-------------------------
# entrada = float(input("Introduce un número "))
# reserva:float = entrada
# i:int = 1
# while entrada >10:
#     entrada = entrada/10
#     i=i+1

# print(f"El número {int(reserva)} tiene {i} dígitos")
#-------------------------PyNative Solution-------------------------
# num = 75869
# count = 0
# while num != 0:
#     # floor division
#     # to reduce the last digit from number
#     num = num // 10

#     # increment counter by 1
#     count = count + 1
# print("Total digits are:", count)


#-----------------------------------EXERCICE 7-----------------------------------
# Print the following pattern

# Write a program to use for loop to print the following reverse number pattern

# 5 4 3 2 1 
# 4 3 2 1 
# 3 2 1 
# 2 1 
# 1
#-------------------------My Solution-------------------------
# numbers:list[int]=[5,4,3,2,1]
# total:int=len(numbers)
# i:int=0
# while total > i:
#     print(f"{numbers}")
#     i=i+1
#     numbers.pop(0)
#-------------------------PyNative Solution-------------------------
# n = 5
# k = 5
# for i in range(0,n+1):
#     for j in range(k-i,0,-1):
#         print(j,end=' ')
#     print()


#-----------------------------------EXERCICE 8-----------------------------------

# Print list in reverse order using a loop

# Given:

# list1 = [10, 20, 30, 40, 50]

# Expected output:

# 50
# 40
# 30
# 20
# 10

#-------------------------My Solution-------------------------
# numbers:list[int] = [10, 20, 30, 40, 50]
# i:int=len(numbers)
# j:int=0
# while i>j:
#     i=i-1
#     print(f"{numbers[i]}")
#-------------------------PyNative Solution-------------------------
# list1 = [10, 20, 30, 40, 50]
# # reverse list
# new_list = reversed(list1)
# # iterate reversed list
# for item in new_list:
#     print(item)
#       ---------------Solution 2---------------
# list1 = [10, 20, 30, 40, 50]
# # get list size
# # len(list1) -1: because index start with 0
# # iterate list in reverse order
# # star from last item to first
# size = len(list1) - 1
# for i in range(size, -1, -1):
#     print(list1[i])

#-----------------------------------EXERCICE 9-----------------------------------
# Display numbers from -10 to -1 using for loop

# Expected output:

# -10
# -9
# -8
# -7
# -6
# -5
# -4
# -3
# -2
# -1
#-------------------------My Solution-------------------------
# number:int=-10
# i:int=0
# while number<i:
#     print(f"{number}")
#     number=number+1
#-------------------------PyNative Solution-------------------------
# for num in range(-10, 0, 1):
#     print(num)


#-----------------------------------EXERCICE 10-----------------------------------
# Use else block to display a message “Done” after successful execution of for loop

# For example, the following loop will execute without any error.

# Given:

# for i in range(5):
#     print(i)

# Expected output:

# 0
# 1
# 2
# 3
# 4
# Done!
#-------------------------My Solution-------------------------
# lista:list[int]=[1,2,3,4,5]
# durada:int = len(lista)
# i:int=0
# while durada>i:
#     print(f"{lista[i]}")
#     i=i+1
#     if i==durada:
#         print("Done!")
#-------------------------PyNative Solution-------------------------
# for i in range(5):
#     print(i)
# else:
#     print("Done!")


#-----------------------------------EXERCICE 11-----------------------------------
# Write a program to display all prime numbers within a range

# Note: A Prime Number is a number that cannot be made by multiplying other whole numbers. A prime number is a natural number greater than 1 that is not a product of two smaller natural numbers

# Examples:

#     6 is not a prime mumber because it can be made by 2×3 = 6
#     37 is a prime number because no other whole numbers multiply together to make it.

# Given:

# # range
# start = 25
# end = 50

# Expected output:

# Prime numbers between 25 and 50 are:
# 29
# 31
# 37
# 41
# 43
# 47
#-------------------------Pablo's Solution-------------------------
# # PyNative Loop exercises using 'while'
# # https://pynative.com/python-if-else-and-for-loop-exercise-with-solutions/


# # Even if the code is only one line, putting the code in a function
# # with an easy to understand name adds to readability.
# # -----------------------------------------------------------------------------
# def is_divisible_by(num: int, divisor: int) -> bool:
#     '''Returns True if num is divisible by divisor.'''

#     return ((num % divisor) == 0)


# # -----------------------------------------------------------------------------
# def is_prime(num: int) -> bool:
#     '''Returns True if num is a prime number. Returns False otherwise.'''

#     # Special case: Numbers equal or less than zero
#     if (num < 1):
#         return False


#     # Normal case: Numbers equal or greater than one
#     result: bool = True
#     start:  int = 2
#     end:    int = num - 1

#     iter:     int  = start
#     finished: bool = (iter > end)

#     while (not finished):
#         if is_divisible_by(num, iter):
#             result = False

#         iter     = iter + 1
#         finished = (iter > end)

#     return result


# # Instead of printing them directly on the terminal, return a list.
# # It's more functional.
# # -----------------------------------------------------------------------------
# def e11(start: int, end: int) -> list[int]:
#     '''Exercise 11: Write a program to display all prime numbers within a range.'''

#     result:   list[int] = []
#     iter:     int       = start
#     finished: bool      = (iter > end)

#     while (not finished):

#         if is_prime(iter):
#             result.append(iter)

#         iter     = iter + 1
#         finished = (iter > end)

#     return result


# # Main
# # -----------------------------------------------------------------------------
# print(e11(25, 50))
# # -----------------------------------------------------------------------------

#-------------------------PyNative Solution-------------------------
# start = 25
# end = 50
# print("Prime numbers between", start, "and", end, "are:")

# for num in range(start, end + 1):
#     # all prime numbers are greater than 1
#     # if number is less than or equal to 1, it is not prime
#     if num > 1:
#         for i in range(2, num):
#             # check for factors
#             if (num % i) == 0:
#                 # not a prime number so break inner loop and
#                 # look for next number
#                 break
#         else:
#             print(num)

#-----------------------------------EXERCICE 12-----------------------------------
# Display Fibonacci series up to 10 terms

# The Fibonacci Sequence is a series of numbers. The next number is found by adding up the two numbers before it. The first two numbers are 0 and 1.

# For example, 0, 1, 1, 2, 3, 5, 8, 13, 21. The next number in this series above is 13+21 = 34.

# Expected output:

# Fibonacci sequence:
# 0  1  1  2  3  5  8  13  21  34
#-------------------------My Solution-------------------------
# numA:int=0
# numB:int=1
# numC:int=0
# contador:int=0
# while contador < 10:
#     print(numC)
#     numA=numB
#     numB=numC
#     numC=numA+numB
#     contador = contador+1
#-------------------------PyNative Solution-------------------------
# # first two numbers
# num1, num2 = 0, 1

# print("Fibonacci sequence:")
# # run loop 10 times
# for i in range(10):
#     # print next number of a series
#     print(num1, end="  ")
#     # add last two numbers to get next number
#     res = num1 + num2

#     # update values
#     num1 = num2
#     num2 = res

#-----------------------------------EXERCICE 13-----------------------------------
# Find the factorial of a given number

# Write a program to use the loop to find the factorial of a given number.

# The factorial (symbol: !) means to multiply all whole numbers from the chosen number down to 1.

# For example: calculate the factorial of 5

# 5! = 5 × 4 × 3 × 2 × 1 = 120

# Expected output:

# 120
#-------------------------My Solution-------------------------
# number = int(input("Introduce un número para hacer el factorial: "))
# reserva:int = number
# total:int = 1
# while number > 0:
#     total=total*number
#     number=number-1
#     if number==0:
#         print(f"El factorial de {reserva} es {total}")

#-------------------------PyNative Solution-------------------------


#-----------------------------------EXERCICE 14-----------------------------------

#-------------------------My Solution-------------------------

#-------------------------PyNative Solution-------------------------


#-----------------------------------EXERCICE 15-----------------------------------

#-------------------------My Solution-------------------------

#-------------------------PyNative Solution-------------------------


#-----------------------------------EXERCICE 16-----------------------------------

#-------------------------My Solution-------------------------

#-------------------------PyNative Solution-------------------------


#-----------------------------------EXERCICE 17-----------------------------------

#-------------------------My Solution-------------------------

#-------------------------PyNative Solution-------------------------


#-----------------------------------EXERCICE 18-----------------------------------

#-------------------------My Solution-------------------------

#-------------------------PyNative Solution-------------------------