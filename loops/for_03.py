# 1.- Write 3 names with input
# 2.- save them in an array
# 3.- print the list with a "for" loop
# 4.- invert the list
# 5.- pedir 3 números, recorrerlos y sumarlos.
# 6.- recorrer una lista de nombres mostrando su índice dentro de la lista

# 1, 2, 3, 4.-
names: list[str] = []

print("Write your names:")

name1 = input()
names.append(name1)

name2 = input()
names.append(name2)

name3 = input()
names.append(name3)

print()
for name in names:
    print(f"Hola {name}")

names_reversed: list[str] = list(reversed(names))

print()
for name in names_reversed:
    print(f"Adiós {name}")


# -----------------------------------6.- names index-----------------------------------

print()
for name in names:
    print(f"{name} tiene el index {names.index(name)}")


# name_list: list[str] = ['John', 'Mary', 'Lucy']
# index_list: list[int] = [0, 1, 2]
#
# for index in index_list:
#     print(name_list[index])


# -----------------------------------5.- numbers-----------------------------------

numbers: list[int] = []
result: int = 0


print("Write 3 numbers:")
number1 = int(input())
numbers.append(number1)

number2 = int(input())
numbers.append(number2)

number3 = int(input())
numbers.append(number3)

print()
for number in numbers:
    print(f"Your number was {number}")
    result = result+number

print(f"The total amount is {result}")
