# Rock, Paper, Scissors v3
# Use boolean operators

import random

options: tuple[str, str, str] = ["Rock", "Paper", "Scissors"]
matches: tuple[str, str, str] = 'match1', 'match2', 'match3'

# Banner
def Banner() -> None:
    print("Welcome to Rock, Paper, Scissors!")


#Main Function
def Play() -> None:

    user_wins: int = 0
    cpu_wins: int = 0
    draws: int = 0
    for match in matches:

        # User chooses one option
        user_hand: str = input("Choose your hand: ")

        # CPU chooses randomly one option
        cpu_hand: str = random.choice(options)
        print(f"The cpu chose {cpu_hand}.")

        # Declare variables
        user_rock: bool = (user_hand == options[0])
        user_paper: bool = (user_hand == options[1])
        user_scissors: bool = (user_hand == options[2])

        cpu_rock: bool = (cpu_hand == options[0])
        cpu_paper: bool = (cpu_hand == options[1])
        cpu_scissors: bool = (cpu_hand == options[2])


        # Draw
        if user_hand == cpu_hand:
            print("Draw!")
            draws+=1


        # User chooses Rock
        elif user_rock and cpu_scissors:
            print("You win!")
            user_wins += 1

        elif user_rock and cpu_paper:
            print("You lose!")
            cpu_wins += 1


        # User chooses Paper
        elif user_paper and cpu_rock:
            print("You win!")
            user_wins += 1

        elif user_paper and cpu_scissors:
            print("You lose!")
            cpu_wins += 1


        # User chooses Scissors
        elif user_scissors and cpu_paper:
            print("You win!")
            user_wins += 1

        elif user_scissors and cpu_rock:
            print("You lose!")
            cpu_wins += 1


    if user_wins > cpu_wins:
        print(f"The user has won")

    elif cpu_wins > user_wins:
        print(f"The user has lost")

    else:
        print(f"The match ended up in a draw")

    print(f"The user has won {user_wins} times, the cpu has won {cpu_wins} and there were a total of {draws} draws")


Banner()
Play()
